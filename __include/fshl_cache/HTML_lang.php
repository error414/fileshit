<?php
/* --------------------------------------------------------------- *
 *        WARNING: ALL CHANGES IN THIS FILE WILL BE LOST
 *
 *   Source language file: C:\Web\WebData\scripts\fshl\fshl-0.4.13\fshl/lang/HTML_lang.php
 *       Language version: 1.11 (Sign:SHL)
 *
 *            Target file: C:\Web\WebData\scripts\fshl\fshl-0.4.13\fshl/fshl_cache/HTML_lang.php
 *             Build date: Sat 14.5.2005 01:51:54
 *
 *      Generator version: 0.4.7
 * --------------------------------------------------------------- */
class HTML_lang
{
var $trans,$flags,$data,$delim,$class,$keywords;
var $version,$signature,$initial_state,$ret,$quit;
var $pt,$pti,$generator_version;
var $names;

function HTML_lang () {
	$this->version=1.11;
	$this->signature='SHL';
	$this->generator_version='0.4.7';
	$this->initial_state=0;
	$this->trans=array(0=>array(0=>array(0=>10,1=>0),1=>array(0=>11,1=>0),2=>array(0=>11,1=>0),3=>array(0=>2,1=>0),4=>array(0=>1,1=>0),5=>array(0=>0,1=>0)),1=>array(0=>array(0=>0,1=>1),1=>array(0=>0,1=>1),2=>array(0=>0,1=>1)),2=>array(0=>array(0=>0,1=>1),1=>array(0=>3,1=>0),2=>array(0=>4,1=>1),3=>array(0=>4,1=>1),4=>array(0=>6,1=>1),5=>array(0=>6,1=>1),6=>array(0=>11,1=>0),7=>array(0=>11,1=>0)),3=>array(0=>array(0=>8,1=>0),1=>array(0=>12,1=>1),2=>array(0=>9,1=>0),3=>array(0=>11,1=>0),4=>array(0=>11,1=>0),5=>array(0=>3,1=>0)),4=>array(0=>array(0=>8,1=>0),1=>array(0=>9,1=>0),2=>array(0=>5,1=>0),3=>array(0=>11,1=>0),4=>array(0=>11,1=>0),5=>array(0=>3,1=>0)),5=>array(0=>array(0=>12,1=>1)),6=>array(0=>array(0=>8,1=>0),1=>array(0=>9,1=>0),2=>array(0=>7,1=>0),3=>array(0=>11,1=>0),4=>array(0=>11,1=>0),5=>array(0=>3,1=>0)),7=>array(0=>array(0=>12,1=>1)),8=>array(0=>array(0=>12,1=>0),1=>array(0=>11,1=>0),2=>array(0=>11,1=>0),3=>array(0=>8,1=>0)),9=>array(0=>array(0=>12,1=>0),1=>array(0=>11,1=>0),2=>array(0=>11,1=>0),3=>array(0=>9,1=>0)),10=>array(0=>array(0=>0,1=>1),1=>array(0=>11,1=>0),2=>array(0=>11,1=>0),3=>array(0=>10,1=>0)),11=>null);
	$this->flags=array(0=>0,1=>0,2=>0,3=>4,4=>4,5=>8,6=>4,7=>8,8=>4,9=>4,10=>0,11=>8);
	$this->delim=array(0=>array(0=>'<!--',1=>'<?php',2=>'<?',3=>'<',4=>'&',5=>'_COUNTAB'),1=>array(0=>';',1=>'&',2=>'SPACE'),2=>array(0=>'>',1=>'SPACE',2=>'style',3=>'STYLE',4=>'script',5=>'SCRIPT',6=>'<?php',7=>'<?'),3=>array(0=>'"',1=>'>',2=>'\'',3=>'<?php',4=>'<?',5=>'_COUNTAB'),4=>array(0=>'"',1=>'\'',2=>'>',3=>'<?php',4=>'<?',5=>'_COUNTAB'),5=>array(0=>'>'),6=>array(0=>'"',1=>'\'',2=>'>',3=>'<?php',4=>'<?',5=>'_COUNTAB'),7=>array(0=>'>'),8=>array(0=>'"',1=>'<?php',2=>'<?',3=>'_COUNTAB'),9=>array(0=>'\'',1=>'<?php',2=>'<?',3=>'_COUNTAB'),10=>array(0=>'-->',1=>'<?php',2=>'<?',3=>'_COUNTAB'),11=>null);
	$this->ret=12;
	$this->quit=13;
	$this->names=array(0=>'OUT',1=>'ENTITY',2=>'TAG',3=>'inTAG',4=>'CSS',5=>'TO_CSS',6=>'JAVASCRIPT',7=>'TO_JAVASCRIPT',8=>'QUOTE1',9=>'QUOTE2',10=>'COMMENT',11=>'TO_PHP',12=>'_RET',13=>'_QUIT');
	$this->data=array(0=>null,1=>null,2=>null,3=>null,4=>null,5=>'CSS',6=>null,7=>'JS',8=>null,9=>null,10=>null,11=>'PHP');
	$this->class=array(0=>null,1=>'html-entity',2=>'html-tag',3=>'html-tagin',4=>'html-tagin',5=>'html-tag',6=>'html-tagin',7=>'html-tag',8=>'html-quote',9=>'html-quote',10=>'html-comment',11=>'xlang');
	$this->keywords=null;
}

/* OUT */
function isd0 () {
$p=$this->pti;
$c1=$this->pt[$p++];
$c2=$c1.$this->pt[$p++];
$c3=$c2.$this->pt[$p++];
$c4=$c3.$this->pt[$p++];
$c5=$c4.$this->pt[$p];
if($c4=='<!--'){
	return array(0,'<!--');
}
if($c5=='<?php'){
	return array(1,'<?php');
}
if($c2=='<?'){
	return array(2,'<?');
}
if($c1=='<'){
	return array(3,'<');
}
if($c1=='&'){
	return array(4,'&');
}
if(stristr("\t\n",$c1)){
	return array(5,$c1);
}
return false;
}

/* ENTITY */
function isd1 () {
$c1=$this->pt[$this->pti];
if($c1==';'){
	return array(0,';');
}
if($c1=='&'){
	return array(1,'&');
}
if(ctype_space($c1)){
	return array(2,$c1);
}
return false;
}

/* TAG */
function isd2 () {
$p=$this->pti;
$c1=$this->pt[$p++];
$c2=$c1.$this->pt[$p++];
$c3=$c2.$this->pt[$p++];
$c4=$c3.$this->pt[$p++];
$c5=$c4.$this->pt[$p++];
$c6=$c5.$this->pt[$p];
if($c1=='>'){
	return array(0,'>');
}
if(ctype_space($c1)){
	return array(1,$c1);
}
if($c5=='style'){
	return array(2,'style');
}
if($c5=='STYLE'){
	return array(3,'STYLE');
}
if($c6=='script'){
	return array(4,'script');
}
if($c6=='SCRIPT'){
	return array(5,'SCRIPT');
}
if($c5=='<?php'){
	return array(6,'<?php');
}
if($c2=='<?'){
	return array(7,'<?');
}
return false;
}

/* inTAG */
function isd3 () {
$p=$this->pti;
$c1=$this->pt[$p++];
$c2=$c1.$this->pt[$p++];
$c3=$c2.$this->pt[$p++];
$c4=$c3.$this->pt[$p++];
$c5=$c4.$this->pt[$p];
if($c1=='"'){
	return array(0,'"');
}
if($c1=='>'){
	return array(1,'>');
}
if($c1=='\''){
	return array(2,'\'');
}
if($c5=='<?php'){
	return array(3,'<?php');
}
if($c2=='<?'){
	return array(4,'<?');
}
if(stristr("\t\n",$c1)){
	return array(5,$c1);
}
return false;
}

/* CSS */
function isd4 () {
$p=$this->pti;
$c1=$this->pt[$p++];
$c2=$c1.$this->pt[$p++];
$c3=$c2.$this->pt[$p++];
$c4=$c3.$this->pt[$p++];
$c5=$c4.$this->pt[$p];
if($c1=='"'){
	return array(0,'"');
}
if($c1=='\''){
	return array(1,'\'');
}
if($c1=='>'){
	return array(2,'>');
}
if($c5=='<?php'){
	return array(3,'<?php');
}
if($c2=='<?'){
	return array(4,'<?');
}
if(stristr("\t\n",$c1)){
	return array(5,$c1);
}
return false;
}

/* TO_CSS */
function isd5 () {
$c1=$this->pt[$this->pti];
if($c1=='>'){
	return array(0,'>');
}
return false;
}

/* JAVASCRIPT */
function isd6 () {
$p=$this->pti;
$c1=$this->pt[$p++];
$c2=$c1.$this->pt[$p++];
$c3=$c2.$this->pt[$p++];
$c4=$c3.$this->pt[$p++];
$c5=$c4.$this->pt[$p];
if($c1=='"'){
	return array(0,'"');
}
if($c1=='\''){
	return array(1,'\'');
}
if($c1=='>'){
	return array(2,'>');
}
if($c5=='<?php'){
	return array(3,'<?php');
}
if($c2=='<?'){
	return array(4,'<?');
}
if(stristr("\t\n",$c1)){
	return array(5,$c1);
}
return false;
}

/* TO_JAVASCRIPT */
function isd7 () {
$c1=$this->pt[$this->pti];
if($c1=='>'){
	return array(0,'>');
}
return false;
}

/* QUOTE1 */
function isd8 () {
$p=$this->pti;
$c1=$this->pt[$p++];
$c2=$c1.$this->pt[$p++];
$c3=$c2.$this->pt[$p++];
$c4=$c3.$this->pt[$p++];
$c5=$c4.$this->pt[$p];
if($c1=='"'){
	return array(0,'"');
}
if($c5=='<?php'){
	return array(1,'<?php');
}
if($c2=='<?'){
	return array(2,'<?');
}
if(stristr("\t\n",$c1)){
	return array(3,$c1);
}
return false;
}

/* QUOTE2 */
function isd9 () {
$p=$this->pti;
$c1=$this->pt[$p++];
$c2=$c1.$this->pt[$p++];
$c3=$c2.$this->pt[$p++];
$c4=$c3.$this->pt[$p++];
$c5=$c4.$this->pt[$p];
if($c1=='\''){
	return array(0,'\'');
}
if($c5=='<?php'){
	return array(1,'<?php');
}
if($c2=='<?'){
	return array(2,'<?');
}
if(stristr("\t\n",$c1)){
	return array(3,$c1);
}
return false;
}

/* COMMENT */
function isd10 () {
$p=$this->pti;
$c1=$this->pt[$p++];
$c2=$c1.$this->pt[$p++];
$c3=$c2.$this->pt[$p++];
$c4=$c3.$this->pt[$p++];
$c5=$c4.$this->pt[$p];
if($c3=='-->'){
	return array(0,'-->');
}
if($c5=='<?php'){
	return array(1,'<?php');
}
if($c2=='<?'){
	return array(2,'<?');
}
if(stristr("\t\n",$c1)){
	return array(3,$c1);
}
return false;
}

}
?>
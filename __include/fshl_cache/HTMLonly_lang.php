<?php
/* --------------------------------------------------------------- *
 *        WARNING: ALL CHANGES IN THIS FILE WILL BE LOST
 *
 *   Source language file: C:\Web\WebData\scripts\fshl\fshl-0.4.13\fshl/lang/HTMLonly_lang.php
 *       Language version: 1.10 (Sign:SHL)
 *
 *            Target file: C:\Web\WebData\scripts\fshl\fshl-0.4.13\fshl/fshl_cache/HTMLonly_lang.php
 *             Build date: Sat 14.5.2005 01:51:54
 *
 *      Generator version: 0.4.7
 * --------------------------------------------------------------- */
class HTMLonly_lang
{
var $trans,$flags,$data,$delim,$class,$keywords;
var $version,$signature,$initial_state,$ret,$quit;
var $pt,$pti,$generator_version;
var $names;

function HTMLonly_lang () {
	$this->version=1.10;
	$this->signature='SHL';
	$this->generator_version='0.4.7';
	$this->initial_state=0;
	$this->trans=array(0=>array(0=>array(0=>6,1=>0),1=>array(0=>2,1=>0),2=>array(0=>1,1=>0),3=>array(0=>0,1=>0)),1=>array(0=>array(0=>0,1=>1),1=>array(0=>0,1=>1),2=>array(0=>0,1=>1)),2=>array(0=>array(0=>0,1=>1),1=>array(0=>3,1=>0)),3=>array(0=>array(0=>4,1=>0),1=>array(0=>7,1=>1),2=>array(0=>3,1=>0),3=>array(0=>5,1=>0)),4=>array(0=>array(0=>7,1=>0)),5=>array(0=>array(0=>7,1=>0)),6=>array(0=>array(0=>0,1=>1),1=>array(0=>6,1=>0)));
	$this->flags=array(0=>0,1=>0,2=>0,3=>4,4=>4,5=>4,6=>0);
	$this->delim=array(0=>array(0=>'<!--',1=>'<',2=>'&',3=>'_COUNTAB'),1=>array(0=>';',1=>'&',2=>'SPACE'),2=>array(0=>'>',1=>'SPACE'),3=>array(0=>'"',1=>'>',2=>'_COUNTAB',3=>'\''),4=>array(0=>'"'),5=>array(0=>'\''),6=>array(0=>'-->',1=>'_COUNTAB'));
	$this->ret=7;
	$this->quit=8;
	$this->names=array(0=>'OUT',1=>'ENTITY',2=>'TAG',3=>'inTAG',4=>'QUOTE1',5=>'QUOTE2',6=>'COMMENT',7=>'_RET',8=>'_QUIT');
	$this->data=array(0=>null,1=>null,2=>null,3=>null,4=>null,5=>null,6=>null);
	$this->class=array(0=>null,1=>'html-entity',2=>'html-tag',3=>'html-tagin',4=>'html-quote',5=>'html-quote',6=>'html-comment');
	$this->keywords=null;
}

/* OUT */
function isd0 () {
$p=$this->pti;
$c1=$this->pt[$p++];
$c2=$c1.$this->pt[$p++];
$c3=$c2.$this->pt[$p++];
$c4=$c3.$this->pt[$p];
if($c4=='<!--'){
	return array(0,'<!--');
}
if($c1=='<'){
	return array(1,'<');
}
if($c1=='&'){
	return array(2,'&');
}
if(stristr("\t\n",$c1)){
	return array(3,$c1);
}
return false;
}

/* ENTITY */
function isd1 () {
$c1=$this->pt[$this->pti];
if($c1==';'){
	return array(0,';');
}
if($c1=='&'){
	return array(1,'&');
}
if(ctype_space($c1)){
	return array(2,$c1);
}
return false;
}

/* TAG */
function isd2 () {
$c1=$this->pt[$this->pti];
if($c1=='>'){
	return array(0,'>');
}
if(ctype_space($c1)){
	return array(1,$c1);
}
return false;
}

/* inTAG */
function isd3 () {
$c1=$this->pt[$this->pti];
if($c1=='"'){
	return array(0,'"');
}
if($c1=='>'){
	return array(1,'>');
}
if(stristr("\t\n",$c1)){
	return array(2,$c1);
}
if($c1=='\''){
	return array(3,'\'');
}
return false;
}

/* QUOTE1 */
function isd4 () {
$c1=$this->pt[$this->pti];
if($c1=='"'){
	return array(0,'"');
}
return false;
}

/* QUOTE2 */
function isd5 () {
$c1=$this->pt[$this->pti];
if($c1=='\''){
	return array(0,'\'');
}
return false;
}

/* COMMENT */
function isd6 () {
$p=$this->pti;
$c1=$this->pt[$p++];
$c2=$c1.$this->pt[$p++];
$c3=$c2.$this->pt[$p];
if($c3=='-->'){
	return array(0,'-->');
}
if(stristr("\t\n",$c1)){
	return array(1,$c1);
}
return false;
}

}
?>
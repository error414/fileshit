<?php
/* --------------------------------------------------------------- *
 *        WARNING: ALL CHANGES IN THIS FILE WILL BE LOST
 *
 *   Source language file: C:\Web\WebData\scripts\fshl\fshl-0.4.13\fshl/lang/TXT_lang.php
 *       Language version: 1.0 (Sign:SHL)
 *
 *            Target file: C:\Web\WebData\scripts\fshl\fshl-0.4.13\fshl/fshl_cache/TXT_lang.php
 *             Build date: Sat 14.5.2005 01:51:54
 *
 *      Generator version: 0.4.7
 * --------------------------------------------------------------- */
class TXT_lang
{
var $trans,$flags,$data,$delim,$class,$keywords;
var $version,$signature,$initial_state,$ret,$quit;
var $pt,$pti,$generator_version;
var $names;

function TXT_lang () {
	$this->version=1.0;
	$this->signature='SHL';
	$this->generator_version='0.4.7';
	$this->initial_state=0;
	$this->trans=array(0=>array(0=>array(0=>0,1=>0)));
	$this->flags=array(0=>1);
	$this->delim=array(0=>array(0=>'_COUNTAB'));
	$this->ret=1;
	$this->quit=2;
	$this->names=array(0=>'OUT',1=>'_RET',2=>'_QUIT');
	$this->data=array(0=>null);
	$this->class=array(0=>null);
	$this->keywords=array(0=>'txt-keywords',1=>array('all'=>1,'your'=>1,'base'=>1,'are'=>1,'belong'=>1,'to'=>1,'us'=>1));
}

/* OUT */
function isd0 () {
$c1=$this->pt[$this->pti];
if(stristr("\t\n",$c1)){
	return array(0,$c1);
}
return false;
}

}
?>
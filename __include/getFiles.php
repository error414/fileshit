<?php 
/**
 * zmensovani obrazku a jine upravy (zadne velke)
 * 
 * Copyright (c) 2006, Petr Cada <error414@error414.com>
 * Web: http://www.error414.com/
 * 
 */

/**
 * vraci vypis adresare
 * 
 * * <code> 
 *     $files = new getFiles(ROOT . $dir);
 *     $files->getInfo();
 * </code>  
 */



class getFiles
{
	/**
	 * jmeno adresare ze ktereho se ma cist
	 * 
	 * @var string
	 */ 
	 protected  $nameOfDir = '';
	 
	  
	 /**
	 * jmena prectenehych adresaru
	 * 
	 * @var array
	 */ 
	 public $dir = array();
	 
	 
	 /**
	 * jmena prectenehych souboru
	 * 
	 * @var array
	 */ 
	 public $files = array();
	 
	 
	 
	 public function __construct($dir)
	 {
	     $this->setDir($dir);
	 }
	 
	 
	 
	 /**
	  * vraci informace o souboru
	  *
	  */
	 public function getInfoAboutDir()
	 {
	 	$this->readNow();
	 }
	 
	 
	 /**
	  * nastavi prohledavaci adresar
	  * 
	  * @param string $dir
	  * @return void
	  */
	 
	 protected function setDir($dir)
	 {
	 	if(!getFiles::isDir($dir)){
	 		die('musite nastavit platny adresar pro vypis');
	 	}else {
	 		$this->nameOfDir = $dir;
	 	}
	 }
	 
	 
	 /**
	  * kontroluje jestli existuje adresar existuje
	  *
	  * @param string $dir
	  * @return bool
	  */
	 static public function isDir($dir)
	 {
	 	if(is_dir($dir)){
	 		return true;
	 	}else {
	 		return false;
	 	}
	 }
	 
	 
	 /**
	  * kontroluje jestli existuje soubor existuje
	  *
	  * @param string $file
	  * @return bool
	  */
	 static public function isFile($file)
	 {
	 	if(is_file($file)){
	 		return true;
	 	}else {
	 		return false;
	 	}
	 }
	
	 
	 /**
	  * cte adresar a uklada informace do pole
	  * 
	  * @return void
	  *
	  */
	 protected function readNow()
	 {
	 	$handle = opendir($this->nameOfDir);
	 	
	 	while (($file = readdir($handle))!==false) {

	 	    if(getFiles::isDir($this->nameOfDir . '/' . $file) and $file != '.' and $file != '..'){
            	$file_usr = $file == '/' ? '/' : $file . '/';
                $this->dir[$file_usr]['chmod'] = getFiles::getChmod($this->nameOfDir . '/' . $file);	
    		}elseif ($file != '.' and $file != '..'){
    		    $this->files[$file]['chmod'] = getFiles::getChmod($this->nameOfDir . '/' . $file);
    		    $this->files[$file]['size']  = getFiles::getSize ($this->nameOfDir . '/' . $file);
    		}
		}
		
		closedir($handle);
	 }
	 
	 
	 
	 static public function getChmod($fileName)
	 { 
	     if(getFiles::isDir($fileName) or getFiles::isFile($fileName)){
	         $perms = fileperms($fileName);
	     }else {
	         echo ('nebyl zadan zadny platny soubor nebo adresar: ' . $fileName . '<br>');
	     }
	     
	     $info = '';
	     // Owner
         $info .= (($perms & 0x0100) ? 'r' : '-');
         $info .= (($perms & 0x0080) ? 'w' : '-');
         $info .= (($perms & 0x0040) ?
            (($perms & 0x0800) ? 's' : 'x' ) :
            (($perms & 0x0800) ? 'S' : '-'));

         // Group
         $info .= (($perms & 0x0020) ? 'r' : '-');
         $info .= (($perms & 0x0010) ? 'w' : '-');
         $info .= (($perms & 0x0008) ?
            (($perms & 0x0400) ? 's' : 'x' ) :
            (($perms & 0x0400) ? 'S' : '-'));

         // World
         $info .= (($perms & 0x0004) ? 'r' : '-');
         $info .= (($perms & 0x0002) ? 'w' : '-');
         $info .= (($perms & 0x0001) ?
            (($perms & 0x0200) ? 't' : 'x' ) :
            (($perms & 0x0200) ? 'T' : '-'));

	     
	     return $info;
	     
	 }
	 
	 
	 
	 /**
	  * vraci velikost souboru
	  *
	  * @param string $fileName
	  * @return string
	  */
	 static public function getSize($fileName)
	 {
	     if(getFiles::isFile($fileName)){
	         $size = filesize($fileName);
	         
	         if($size < 1000){
	             $size .= ' B';
	         }elseif ($size > 1000 and $size < 1000000){
	             $size = round($size / 1000, 2) . ' kB';
	         }elseif ($size > 1000000){
	             $size = round($size / 1000000, 2) . ' MB';
	         }
	     }else{
	         $size = 0;
	     }
	     
	     return $size;
	 }
	
}
?>
<?php


/**
 * varti nadrazeny adresar pokud je adresa platna
 * 
 * @param string
 * @return string
 */

function fileUp($patch)
{
    $dirName = '';
    
    $up = modifURL($patch);
    if(!getFiles::isDir(dirname(ROOT . $up)) or $up == '.'){
        $dirName = '';
    }else{
        $dirName = modifURL(dirname($up));
    }
    
    return $dirName;
}





/**
 * autolad pro tridy
 * 
 * @param string
 * @return void
 */

function __autoload($classname)
{
    include_once(dirname(__FILE__) . '/' . $classname . '.php');
}





/**
 * upravz adresu na tvar neco/neco/ nesmi tam byt tecky etc..
 * 
 * @param string
 * @return string
 */

function modifURL($patch)
{
    $savePatch = '';
    
    $url = substr(dirname($patch), 0, 1) == '/' ? substr($patch, 1, strlen($patch)) : $patch;  
    $part = explode('/', $url);

    $nahradit = array("/", "..", ".");
    
    
    $prew = '';
    foreach($part as $value){
    	
        if(!empty($value) and !in_array($value, $nahradit)){
            $savePatch .= eregi_replace($nahradit . '^[_a-zA-Z0-9]+', "",$value) . '/';
            
            if(!getFiles::isDir(ROOT . $savePatch)){
                $savePatch = $prew;
                break;
            }
            $prew = $savePatch; 
        }
    }
    
    
    return $savePatch;
}







/**
 * This source file is subject to the GNU GPL license.
 *
 * @author     David Grudl aka -dgx- <dave@dgx.cz>
 * @link       http://www.dgx.cz/
 * @copyright  Copyright (c) 2006 David Grudl
 * @license    GNU GENERAL PUBLIC LICENSE
 * @category   Text
 * @version    1.0
 */


// charset detection
function detect($s)
{
    if (preg_match('#[\x80-\x{1FF}\x{2000}-\x{3FFF}]#u', $s))
        return 'UTF-8';

    if (preg_match('#[\x7F-\x9F]#', $s))
        return 'WINDOWS-1250';

    return 'ISO-8859-2';
}
?>
<?php

/**
 * trida na zobrazeni zdrojoveho kodu
 * Copyright (c) 2006, Petr Cada <error414@error414.com>
 * Web: http://www.error414.com/
 * 
 */


/**
 * zobrazeni zdrojoveho kodu php
 * 
 * * <code> 
 *     $php_text = new php_prew(nameOfFile);
       $php_text->output();
 * </code>  
 */


class php_prew
{
    
    public $highlight = true;
    static public $language  = 'html';
    
    public function __construct($patch)
    { 
        if(getFiles::isFile(ROOT . $patch)){
            $this->patch = ROOT . $patch;
        }else{
            return false;
        }
       
        $this->readFile();
    }
    
    
    
    protected function readFile()
    {   
        if(empty($this->patch)){
          return false;  
        }
        
        $this->code = $this->setCharset(file_get_contents($this->patch));
    }
    
    
    public function getCode()
    {
        if(!isset($this->code)){
          return false;  
        }else {
            
            $this->code = $this->highlight ? self::highlight($this->code) : htmlspecialchars($this->code);
            
            $code['info'] = $this->info();
            $code['code'] = $this->code;
            return $code;
        }
        
       
    } 
    
    
    public function info()
    {
        $inf['cesta'] = $this->setCharset($this->patch);
        $inf['name']  = $this->setCharset(basename($this->patch));
        $inf['prava'] = getFiles::getChmod($this->patch);
        $inf['size']  = getFiles::getSize($this->patch);
        return $inf;
    }
    
   
    static public function highlight($code)
    {
        include_once('__include/fshl.php');
        $fshl = new fshlParser('HTML_UTF8', P_TAB_INDENT | P_LINE_COUNTER);
            
        $code = $fshl->highlightString(strtoupper(self::$language), $code);
        return $code;
    }
    
    
    public function setCharset($string)
    {
        return iconv(detect($string), 'utf-8', $string);
    }
}


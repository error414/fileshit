<?php

/**
 * trida na zobrazeni zdrojoveho kodu nebo obrazku
 * Copyright (c) 2006, Petr Cada <error414@error414.com>
 * Web: http://www.error414.com/
 * 
 */


/**
 * zobrazeni zdrojoveho kodu php nebo obrazku
 * 
 * * <code> 
 *     $php_text = new php_prew(nameOfFile);
       $php_text->output();
 * </code>  
 */



class prew
{
    /**
     * text chyby.
     *
     * @var string
     */
    protected $error = '';
    
    /**
     * zobrazovat cisla radku pokud jede zvyraznovac
     *
     * @var bool
     */
    public    $line = true;
    
    /**
     * pole pripon ktere fileshit rozeznava
     * $extension[picture]  = array()
     * $extension[multi]    = array()
     * $extension[js]       = array()
     * $extension[css]      = array()
     * $extension[html]     = array()
     * $extension[sql]      = array()
     *
     * @var array
     */
    protected    $extension;
    
    
    /**
     * text ktery obsahuje otevreny soubor
     *
     * @var array
     */
    protected $code;
    
    
    
    /**
     * konstruktor vzoru factory
     * rozhoduje kterou tridu pouzit na zobrazeni nebo stazeni souboru
     *
     * @param string $patch
     * @return bool
     */
    public function __construct($patch)
    {
        if(!getFiles::isFile(ROOT . $patch)){
            $this->setError('Cesta ' . $patch . 'neni platna pro žádný soubor');
            return false;
        }
        
        $this->initExtension();
        
        if(in_array($this->getEx($patch), $this->extension['multi'])){ 
                $univer = new php_prew($patch);
                $this->code = $univer->getCode();
        
        }elseif (in_array($this->getEx($patch), $this->extension['css'])){
                $univer = new php_prew($patch);
                php_prew::$language = 'css';
                $this->code = $univer->getCode();
        
        }elseif (in_array($this->getEx($patch), $this->extension['js'])){
                $univer = new php_prew($patch);
                php_prew::$language = 'js';
                $this->code = $univer->getCode();
               
        }elseif (in_array($this->getEx($patch), $this->extension['text'])){ 
                $univer = new php_prew($patch);
                $univer->highlight = false;
                $this->code = $univer->getCode();
        
        }elseif (in_array($this->getEx($patch), $this->extension['sql'])){ 
                $univer = new php_prew($patch);
                php_prew::$language = 'sql';
                $this->code = $univer->getCode();
        }else{
                $this->seterror('nezanama pripona (dodelavam)');
                return false;
        }
        
         
    }
    
    
    /**
     * zjisteni pripony souboru
     *
     * @param string $patch
     * @return string
     */
    public static function getEx($patch){
        $info = pathinfo($patch);
        return strtolower($info['extension'] ? $info['extension'] : 'unknow');
    }
    
    
    /**
     * nastaveni textu chyby
     *
     * @param string $text
     */
    protected function setError($text){
        $this->error = $text;
    }
    
    
    /**
     * vraci text chyby
     *
     * @return string
     */
    public function getError(){
        return $this->error;
    }
    
    
    /**
     * odeslani na vystup informaci a tela souboru
     *
     */
    public function output()
    {
        include_once(dirname(__FILE__) . '/class.TemplatePower.inc.php');
        /* prirarezi bloků -----------------------------  */
        $tpl = new TemplatePower('tpl/main.tpl');
        $tpl->assignInclude('telo', 'tpl/teloEditor.tpl' );
        /* --------------------------------------------- */ 
        $tpl->prepare();
        
        $tpl->assign('telo',        $this->code['code']);    
        $tpl->assign('info_name',   $this->code['info']['name']);
        $tpl->assign('info_size',   $this->code['info']['size']);
        $tpl->assign('info_prava',  $this->code['info']['prava']);
        $tpl->assign('info_cesta',  $this->code['info']['cesta']);
        
        $tpl->printToscreen();
    }
    
   /**
    * inicialiuace znamych pripon
    *
    */
   public function initExtension()
   {
        $this->extension['picture'] = array('gif', 'png', 'jpg');
        $this->extension['multi']   = array('hmtl','htm', 'php', 'php3', 'hta');
        $this->extension['js']      = array('js');
        $this->extension['css']     = array('css');
        $this->extension['text']    = array('txt', 'htaccess', 'unknow');
        $this->extension['sql']     = array('db', 'sql');
   }
        

   
}
<?php

/**
 * zmensovani obrazku a jine upravy (zadne velke)
 * 
 * Copyright (c) 2006, Petr Cada <error414@error414.com>
 * Web: http://www.error414.com/
 * 
 */
 
 
 /**
 * sesivacka - znemseni obrazku png, gif, jpg pokud jsou podporovanz v gd2 knihovne
 *
 * <code> 
 *     $ses = new sesivacka();
 *     $ses->tmpDir = 'tmp/';
 *     $ses->doIt('img/obloha.jpg');
 * </code>
 */
 
 class sesivacka
 {
    
    /**
     * adresar pro ukladani vzgenerovanych obrazku
     * @var string
     */
    public $tmpDir = 'tmp/';
    
    
    
    
    /**
     * obrazek ketry znensujeme, musi
     * byt pouzita cela cesta
     *      
     * adr/jinyAdr/jemeno.png          
     * 
     *@var string
     */
    public $img;
    
    
    
    /**
     * seznam podporovanych formatu obrazku
     *      
     *@var array
     */
    private $supportImgType = array(
                                'image/gif',
                                'image/jpeg',
                                'image/png');
                                
                                


    /**
     * informace o obrazku 
     * stejene pole jako vraci getimagesize()     
     *      
     *@var array
     */
   public $imageInfo = array();
   
   
   
   
   
   /**
     * pozadovana sirka zmenseneho obrazku  
     *      
     *@var int
     */
   public $newImageWidth = 200;
   
   
   
   
   
   
   /**
     * vyska zmenseneho obrazku  
     *      
     *@var int
     */
   private $newImageHeigh;
   
   
   
   /**
     * jmeno zmenseneho obrazku 
     *      
     *@var int
     */
   private $tmpNamePatch;
    
    
    
  
   /**
     * efekty obrayku
     * cerna - bila; defaultne vypnuto    
     *      
     *@var bool
     */
   public $imageEfektGreyScale = false;
   
   
   
   
   /**
     * povoleni vytvoreni zvetseneho obrazku  
     *      
     *@var bool
     */
   public $enableResizeOverSize = false;
    
    
    
    public function __construct()
    {
        if (version_compare(PHP_VERSION , '5.0.3', '<')){
            die('sesivacka potrebuje PHP ve verzi 5.0.3 nebo novejsi');
        }
        
        $this->checkModule();
    
    
    }
    
    
    
    /**
     * kontrola existence rizsireni pro praci s obrazky
     * neuspech ukoncuje script          
     */
    private function checkModule(){
        if(!extension_loaded('gd')){
            die('Neni nacteno rozsireni pro praci s obrazky GD');
        }
    }
    
    
    
    /**
     * zapracovani obrazku
     * neuspech ukoncuje script          
     */
    public function doIt($img = ''){
        if(empty($img)){
            die($this->error('musite zadat obrazek ke zpracovani'));
        }
        
        $imageType = $this->isImg($img);
        
        switch($imageType)
        {
            case $this->supportImgType[0]:
                    return $this->resizeGif($img);
                    break;
            
            case $this->supportImgType[1]:
                    return $this->resizeJPG($img);
                    break;
            
            case $this->supportImgType[2]:
                    return $this->resizePNG($img);
                    break;
            
            default:
                   die($this->error('Tento typ obrazku neni podporovan -- chyba 10'));
        }
    
    
    }
    
    
    
    /**
     * kontrola existence obrazku a zpravneho mime
     * vraci mime typ obrazku 
     *     
     * return string             
     */
    private function isImg($img){
        if(!@file_exists($img)){
            die($this->error('Nebyl nalezen soubor s obrazkem'));
        }
        $this->imageInfo = getimagesize($img);
        
        
        if(!in_array($this->imageInfo['mime'], $this->supportImgType)){
            die($this->error('Tento typ obrazku neni podporovan'));
        }
        
        return $this->imageInfo['mime'];
    }
    
    
    
    
    /**
     * zmenseni obrazku PNG
     * vraci adresu vygenerovaneho obrazku     
     *     
     * return string             
     */
    private function resizePNG($img){
        if(!$this->isSuport(IMG_PNG)){
            die('Knihovna GD nepodporuje obrazky type PNG');
        }
        
        if($this->resizenExist($img)){
            return $this->resizenExist($img);
        }else{
            $fp = imagecreatefrompng($img);
            $fx = imagecreatetruecolor ($this->newImageWidth,$this->newImageHeight);
            imagecopyresized ($fx,$fp,0,0,0,0,$this->newImageWidth, $this->newImageHeight, $this->imageInfo[0],$this->imageInfo[1]);
            
            if($this->imageEfektGreyScale)
                $this->greyScale($fx);
            
            imagePNG ($fx, $this->tmpDir . $this->tmpNamePatch);
            
            imagedestroy($fx);
             imagedestroy($fp);
             
             return $this->tmpDir . $this->tmpNamePatch;
        }
    
    }
    
    
    
    
    /**
     * zmenseni obrazku JPG
     * vraci adresu vygenerovaneho obrazku     
     *     
     * return string             
     */
    private function resizeJPG($img){
        if(!$this->isSuport(IMG_JPEG)){
            die($this->error('Knihovna GD nepodporuje obrazky type JPG'));
        }
        
        if($this->resizenExist($img)){
            return $this->resizenExist($img);
        }else{
            $fp = imagecreatefromjpeg($img);
            $fx = imagecreatetruecolor ($this->newImageWidth,$this->newImageHeight);
            imagecopyresized ($fx,$fp,0,0,0,0,$this->newImageWidth, $this->newImageHeight, $this->imageInfo[0],$this->imageInfo[1]);
            
            if($this->imageEfektGreyScale)
                $this->greyScale($fx);
            
            imagejpeg ($fx, $this->tmpDir . $this->tmpNamePatch);
            
            imagedestroy($fx);
             imagedestroy($fp);
             
             return $this->tmpDir . $this->tmpNamePatch;
        }
    
    }
    
    
    
    /**
     * zmenseni obrazku GIF
     * vraci adresu vygenerovaneho obrazku     
     *     
     * return string             
     */
    private function resizeGIF($img){
        if(!$this->isSuport(IMG_GIF)){
            die('Knihovna GD nepodporuje obrazky type JPG');
        }
        
        if($this->resizenExist($img)){
            return $this->resizenExist($img);
        }else{
            $fp = imagecreatefromgif($img);
            $fx = imagecreatetruecolor ($this->newImageWidth,$this->newImageHeight);
            
            imagecopyresampled ($fx,$fp,0,0,0,0,$this->newImageWidth, $this->newImageHeight, $this->imageInfo[0],$this->imageInfo[1]);

            if($this->imageEfektGreyScale)
                $this->greyScale($fx);
            
            imagegif ($fx, $this->tmpDir . $this->tmpNamePatch);
            
            imagedestroy($fx);
            imagedestroy($fp);
             
             return $this->tmpDir . $this->tmpNamePatch;
        }
    
    }
    
    
    
    /**
     * zjisteni jestli je typ obrazku podporovan   
     *     
     * return bools             
     */
    public function isSuport($type = '')
    {
        if(imagetypes() & $type){
            return true;
        }else{
            return false;
        }
    }
    
    
    
    
    /**
     * rozparsovani adresy obrazku
     * varci se pole ve stejnem formatu jako funkce pathinfo() 
     *     
     * return array           
     */
    public function parseImgPatch($img)
    {
        $path_parts = pathinfo($img);
        return $path_parts;
    }
    
    
    
    
    /**
     * vypocet vysky obrazku 
     *       
     * return int          
     */
    public function newImgSize()
    {
        if($this->imageInfo[1] === 0){
            die($this->error('chyba pri vypoctu velikosti obrazku'));
        }
        
        $this->newImageHeight = round($this->newImageWidth / ($this->imageInfo[0]/$this->imageInfo[1]));
        return $this->newImageHeight;
    }
    
    
    
    
    /**
     * zjisteni jestli zmenseny obrazek uz neexistuje 
     *     
     * return mixed           
     */
    private function resizenExist($img)
    {
        $patchInfoImg = $this->parseImgPatch($img);
        
        $this->newImgSize();
        
        if($this->newImageWidth > $this->imageInfo[0] and $this->enableResizeOverSize == 0){;
            return $img;
        }elseif($this->newImageWidth == $this->imageInfo[0] and $this->imageEfektGreyScale == 0){
            return $img;
        }else{
            $this->tmpNamePatch = '~' . $patchInfoImg['basename']
                        . $this->newImageWidth . 'X' . $this->newImageHeight
                        . '[' . $this->imageEfektGreyScale . ',0,0,0,0,0,0]' . '.' . $patchInfoImg['extension'];
        
        
            if(@file_exists($this->tmpDir . $this->tmpNamePatch)){
                return $this->tmpDir . $this->tmpNamePatch;
            }else{
                return false;
            }
        }
    }
    
    
    
    
    /**
     * zmeni paletu obrazku na greyscale
     *     
     * return void           
     */
    private function greyScale($fx, $dither=1) {   
        if (!($t = imagecolorstotal($fx))) {
            $t = 256;
            imagetruecolortopalette($fx, $dither, $t);   
        }
        for ($c = 0; $c < $t; $c++) {   
            $col = imagecolorsforindex($fx, $c);
            $min = min($col['red'],$col['green'],$col['blue']);
            $max = max($col['red'],$col['green'],$col['blue']);
            $i = ($max+$min)/2;
            imagecolorset($fx, $c, $i, $i, $i);
        }
    }
   
   
   
   /**
     * vytvoreni chyboveho hlaseni do obrazku
     *     
     * return void           
     */
    private function error($text) {
    
        if(!$this->isSuport(IMG_JPG)){
            die('Knihovna GD nepodporuje obrazky type PNG');
        }
         
        $im = imagecreatetruecolor(400, 100);
        
        $bg = imagecolorallocate($im, 255, 255, 255);
        $textcolor = imagecolorallocate($im, 225, 225, 225);
        
        imagestring($im, 5, 10, 45, $text, $textcolor);
        
        imagejpeg($im, $this->tmpDir . 'error.jpeg');
        
        die ('<img src="' . $this->tmpDir . 'error.jpeg" >');

   }
   
   
   public function initExtension()
   {
       $extension['picture'] = array('gif', 'png', 'jpg');
       $extension['multi']   = array('hmtl','htm', 'php', 'php3', 'hta');
       $extension['js']      = array('js');
       $extension['css']     = array('css');
       $extension['text']    = array('txt');
   }

}

?>
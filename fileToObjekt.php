<?php
/**
 * zmensovani obrazku a jine upravy (zadne velke)
 * 
 * Copyright (c) 2006, Petr Cada <error414@error414.com>
 * Web: http://www.error414.com/
 * 
 */

/**
 * prevede soubor na obkjekt
 * 
 * <code>
 * $fi = fileToObjekt::getInstance('config.php');
 * </code>   
 */


class filetoObjekt
{

    /**
     * nazev souboru s cestou
     * @var string     
     */         
    private $file = '';
    
    
    /**
     * velikost souboru
     * @var string     
     */  
    private $size = '';
    
    
    /**
     * nazev souboru beze cesty
     * @var string     
     */  
    private $name = '';
    
    
    
    
    /**
     * pole obrazku pro ktere uz objetkty existuji, ulozenz i handlery na instance
     * instance => nazev
     *     
     * @param array
     */
     static private $willCreate = array();          
    
    
    
    /**
     * privatni kostruktor vzoru singlenton
     */
    private function __construct()
    {
    }
    
    
    
    /**
     * vraci instaci tridy odpovidajici zadanemu souboru
     * 
     *@param  string
     *@retunt object          
     */
    public function getInstance($file)
    {
        
        if(!array_key_exists($file, self::$willCreate)){
             self::$willCreate[$file]       = new filetoObjekt($file);
             self::$willCreate[$file]->file = $file;
             
             // naplneni vlastnostni tridy
             self::$willCreate[$file]->listParam();
        }
        
        return  self::$willCreate[$file];
    }
        
    
    /**
     * naplneni vlastnosti
     * 
     *@return void
     */
    private function listParam()
    {
          $path_parts = pathinfo($this->file); 
          
          $this->size = @filesize($this->file);
          $this->name = basename($this->file);
          $this->type = $path_parts['extension'];
    }  
    

    
    
    /**
     * debugovaci metoda vypise pole willCreateObkejtForFile;
     * 
     *          
     */
    public function debug()
    {
         print_r(self::$willCreate);
    }
    
    
    
    /**
     * vraci jmeno souboru bez cesty
     * 
     *@retunt string          
     */
    public function name()
    {
        return $this->name;
    }

    
    /**
     * vraci type souboru
     * 
     *@retunt string          
     */
    public function type()
    {
        return $this->type;
    }
    
    
    
    
    
    /**
     * vraci velikost souboru
     * 
     *@return string      
     */
    public function size()
    {
        $count = 0;
        $format = array("B","KB","MB","GB","TB","PB","EB","ZB","YB");
        while(($this->size/1024) > 1 && $count < 8){
            $this->size = $this->size / 1024;
            $count++;
        }
        return number_format($this->size,0,'','.')." ".$format[$count];
    }
    
    
    public function __destruct2()
    {
        unset(self::$willCreate[$this->file]);
    }
    
    public function __destruct()
    {
        echo 'nicim';
    }
}
?>

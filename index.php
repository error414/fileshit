<?php
include_once(dirname(__FILE__) . '/__include/config.php' );
ini_set('display_errors', ERRORS);

include_once(dirname(__FILE__) . '/__include/class.TemplatePower.inc.php');
include_once(dirname(__FILE__) . '/__include/lang.php');
include_once(dirname(__FILE__) . '/__include/knihovna.php');

/* prirarezi bloků -----------------------------  */
$tpl = new TemplatePower('tpl/main.tpl');
$tpl->assignInclude('telo', 'tpl/telo.tpl' );
/* --------------------------------------------- */
$tpl->prepare();


$ses = new sesivacka();



echo $dirName = iconv("UTF-8", "windows-1250", $_GET['dir']);
//$dirName = 'data/.svn';


$dir = modifURL($dirName);

echo ' - ' . $dir;

$tpl->assignGlobal('fileDir', $dir);
$tpl->assignGlobal('title'  , TITLE);
$tpl->assignGlobal('paticka', PATICKA);
$tpl->assignGlobal('fileUp', fileUp($dir));

$files = new getFiles(ROOT . $dir);
$files->getInfoAboutDir();



foreach($files->dir as $key => $value){
    $tpl->newBlock('dir');
    $tpl->assign('fileName',  iconv(detect($key), "UTF-8", $key));
    $tpl->assign('filePatch', modifURL($dir . $key));
    $tpl->assign('filePerms', $value['chmod']);
    $tpl->assign('root'  , ROOT);  
}


foreach($files->files as $key => $value){
    
    $tpl->newBlock('file');
    $tpl->assign('fileName',  iconv(detect($key), "UTF-8", $key));
    
    $path_parts = pathinfo($key);
    if(in_array($path_parts['extension'], $extensionPicture)){
        $ses->tmpDir = 'tmp/';
        $ses->newImageWidth = 80;
        $ses->enableResizeOverSize = false;
        $tpl->assign('nahled',  '<img src="'. $ses->doIt(ROOT . $dir . $key) . '">');
    }
    
    $tpl->assign('fileSize',  $value['size']);
    $tpl->assign('filePerms', $value['chmod']);
}


$tpl->printToscreen();
?>